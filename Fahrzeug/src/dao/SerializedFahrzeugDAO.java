package dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import fahrzeuge.Fahrzeug;

public class SerializedFahrzeugDAO implements FahrzeugDAO{
	
	public static String dbpath = "";
	public ArrayList<Fahrzeug> fahrzeugList;
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
	/**
	 * parameter constructor 
	 */
	@SuppressWarnings("static-access")
	public SerializedFahrzeugDAO(String path){
		super();
		this.dbpath = path;
		File file = new File(path);
		if (!file.exists()) {
			try {
				file.createNewFile();
				writeFahrzeugListToFile(new ArrayList<Fahrzeug>());
			} catch (IOException e) {
				System.out.println("Datei konnte nicht erzeugt werden! " +e);
			}
		}
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
	@Override
	public ArrayList<Fahrzeug> getFahrzeugList() {
		if (fahrzeugList == null) {
			fahrzeugList = LoadFahrzeugList();
		}
		return fahrzeugList;
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
	@Override
	public Fahrzeug getFahrzeugbyId(int id) {
		
		for (Fahrzeug fahrzeug : getFahrzeugList()) {
			if (fahrzeug.getId() == id) {
				return fahrzeug;
			}
		}
		return null;
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++		
	@Override
	public void speichereFahrzeug(Fahrzeug fahrzeug){
		if (getFahrzeugbyId(fahrzeug.getId()) == null) {
			getFahrzeugList().add(fahrzeug);
			persistChanges();
		} else {
			throw new IllegalArgumentException("Diese Id "
					+ fahrzeug.getId() + " existiert bereits");
		}
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++		
	@Override
	public void loescheFahrzeug(Fahrzeug fahrzeug) {
		if (getFahrzeugbyId(fahrzeug.getId()) != null) {
			
			for (int i = 0; i < getFahrzeugList().size(); i++) {

				if (getFahrzeugList().get(i).getId() == fahrzeug.getId()) {
					getFahrzeugList().remove(getFahrzeugList().get(i));
					// nach dem loschen serialize
					persistChanges();
				}
			}

		} else {
			throw new IllegalArgumentException("Fahrzeug mit ID : "
					+ fahrzeug.getId() + " existiert nicht.");
		}
		
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++		
	@Override
	public void aktualisiereFahrzeug(Fahrzeug fahrzeug) {
		if (getFahrzeugbyId(fahrzeug.getId()) != null) {
			for (int i = 0; i < getFahrzeugList().size(); i++) {

				if (getFahrzeugList().get(i).getId() == fahrzeug.getId()) {
					getFahrzeugList().remove(getFahrzeugList().get(i));
				}
			}

			getFahrzeugList().add(fahrzeug);
			persistChanges();
		} else {
			throw new IllegalArgumentException("Fahrzeug mit ID : "
					+ fahrzeug.getId() + " existiert nicht.");
		}
		
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	
	private static void writeFahrzeugListToFile(ArrayList<Fahrzeug> fahrzeug) {
		try {
			FileOutputStream fos = new FileOutputStream(dbpath);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(fahrzeug);
			oos.close();
		} catch (FileNotFoundException e) {
			System.out.println("File kann nicht gefunden werden: " + dbpath);
		} catch (IOException e) {
			System.out.println("Es ist nicht moglich zu schreiben in file: " + dbpath);
		}
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++		
		@SuppressWarnings("unchecked")
		private ArrayList<Fahrzeug> readFahrzeugListFromFile() {
			try {
				FileInputStream fis = new FileInputStream(dbpath);
				ObjectInputStream ois = new ObjectInputStream(fis);
				ArrayList<Fahrzeug> fahrzeug = (ArrayList)ois.readObject();
				ois.close();
				return fahrzeug;
			} catch (FileNotFoundException e) {
				System.out.println("File konnte nicht gefunden werden: " + dbpath);
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Es kann nicht von file gelesen werden: " + dbpath);
			} catch (ClassNotFoundException e) {
				System.out.println("Klasse kann nicht gefunden werden: "
						+ dbpath);
			}

			return null;
		}
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++		
		private ArrayList<Fahrzeug> LoadFahrzeugList() {
			ArrayList<Fahrzeug> fahrzeug = readFahrzeugListFromFile();
			
			if (fahrzeug == null) {
				fahrzeug = new ArrayList<Fahrzeug>();
				writeFahrzeugListToFile(fahrzeug);
			}
			return fahrzeug;
		}
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++		
		private void persistChanges() {
			writeFahrzeugListToFile(this.fahrzeugList);
		}
		
		
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++		
}
