package dao;

import java.util.ArrayList;

import fahrzeuge.Fahrzeug;

public interface FahrzeugDAO {
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++		
	/**
	 * gibt zuruck alle persistent gespeicherte Fahrzeugobjekte.  
	 * @return ArrayList<Fahrzeug>
	 */
	public ArrayList<Fahrzeug> getFahrzeugList();
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++		
	/**
	 * gibt Fahrzeug Objekt zuruck anhand von id
	 * @param id
	 * @return Fahrzeug fahrzeug
	 */
	public Fahrzeug getFahrzeugbyId(int id);
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++		
	/**
	 * speichert Fahrzeug Objekt
	 * @param Fahrzeug fahrzeug 
	 */
	public void speichereFahrzeug(Fahrzeug fahrzeug);
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++		
	/**
	 * loscht Fahrzeug Objekt
	 * @param Fahrzeug fahrzeug
	 */
	public void loescheFahrzeug(Fahrzeug fahrzeug);
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++		
	/**
	 * aktualisiert Fahrzeug Objekt
	 * @param Fahrzeug fahrzeug
	 */
	public void aktualisiereFahrzeug(Fahrzeug fahrzeug);
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++		

}
