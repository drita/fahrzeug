package management;

import java.util.ArrayList;

import dao.FahrzeugDAO;
import fahrzeuge.Fahrzeug;
import fahrzeuge.Lkw;
import fahrzeuge.Pkw;

public class FahrzeugManagement {

	FahrzeugDAO dao;

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	public FahrzeugManagement(FahrzeugDAO serializedFahrzeugDAO) {
		dao = serializedFahrzeugDAO;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * die methode braucht als Parameter einen Fahrzeug
	 * und gibt zuruck einen String mit Fahzeug Daten
	 * @param Fahrzeug fahrzeug
	 * @return String Fahrzeug
	 */
	public String alleDatenAusgeben(Fahrzeug fahrzeug) {
		if (fahrzeug != null) {
			return dao.getFahrzeugbyId(fahrzeug.getId()).toString();
		} else {
			throw new IllegalArgumentException("Bitte geben sie Fahrzeug ein ");
		}

	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * die methode braucht als Parameter einen Fahrzeug
	 * dann findet er im Dao den fahrzeug und andert die daten
	 * @param fahrzeug
	 */
	public void fahrzeugDatenAndern(Fahrzeug fahrzeug) {

		if (fahrzeug != null) {
			dao.aktualisiereFahrzeug(fahrzeug);
		} else {
			throw new IllegalArgumentException("Bitte geben sie Fahrzeug ein ");
		}
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * Die methode fugt eine fahrzeug hinzu
	 * @param Fahrzeug fahrzeug
	 */
	public void fahrzeugeHinzufugen(Fahrzeug fahrzeug) {
		if (fahrzeug != null) {
			dao.speichereFahrzeug(fahrzeug);
		} else {
			throw new IllegalArgumentException("Bitte geben sie Fahrzeug ein ");
		}
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * Die methode loescht den Fahrzeug von Dao
	 * @param Fahrzeug fahrzeug
	 */
	public void fahrzeugeLoschen(Fahrzeug fahrzeug) {
		if (fahrzeug != null) {
			dao.loescheFahrzeug(fahrzeug);
		} else {
			throw new IllegalArgumentException("Bitte geben sie Fahrzeug ein ");
		}
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * Methode gibt den anzahl der Fahrzeuge zuruck
	 * @return int anzahl
	 */
	public int gesamtzahlErfasstenFahrzeuge() {
		if (dao.getFahrzeugList() != null) {
			return dao.getFahrzeugList().size();
		} else {
			throw new IllegalArgumentException(
					"Not Available! ");
		}
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	  * Methode gibt den anzahl der Pkw zuruck
	 * @return int anzahlPkw
	 */
	public int gesamtzahlErfasstenPkw() {
		int anzahlPkw = 0;
		ArrayList<Fahrzeug> fahrzeugList = dao.getFahrzeugList();

		if (fahrzeugList != null) {
			for (Fahrzeug fahrzeug : fahrzeugList) {
				if (fahrzeug instanceof Pkw) {
					anzahlPkw++;
				}
			}
		} else {
			throw new IllegalArgumentException(
					"Not Available! ");
		}
		return anzahlPkw;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 *  * Methode gibt den anzahl der Lkw zuruck
	 * @return int anzahlLkw
	 */
	public int gesamtzahlErfasstenLkw() {
		int anzahlLkw = 0;
		
		if (dao.getFahrzeugList() != null) {
			for (Fahrzeug fahrzeug : dao.getFahrzeugList()) {
				if (fahrzeug instanceof Lkw) {
					anzahlLkw++;
				}
			}
		} else {
			throw new IllegalArgumentException(
					"Not Available! ");
		}
		if (anzahlLkw > 0) {
			return anzahlLkw;
		} else {
			throw new IllegalArgumentException(
					"Not Available! ");
		}
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 *  * Methode gibt den Durschnittsalter der Fahrzeuge zuruck
	 * @return int alter
	 */
	public int durchschnittsalterFahrzeuge() {
		int summeAlter = 0;

		if (dao.getFahrzeugList() != null) {
			for (Fahrzeug fahrzeug : dao.getFahrzeugList()) {
				summeAlter += fahrzeug.getAlter();
			}
		} else {
			throw new IllegalArgumentException(
					"Not Available! ");
		}

		return summeAlter / dao.getFahrzeugList().size();
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * Methode gibt den Durschnittsalter der Pkw zuruck
	 * @return int alterPkw
	
	 */
	public int durchschnittsalterPkw() {
		int summeAlter = 0;
		int anzahlPkw = 0;

		if (dao.getFahrzeugList() != null) {
			for (Fahrzeug fahrzeug : dao.getFahrzeugList()) {
				if (fahrzeug instanceof Pkw) {
					summeAlter += fahrzeug.getAlter();
					anzahlPkw++;
				}
			}
		} else {
			throw new IllegalArgumentException(
					"Not Available! ");
		}
		
		if (anzahlPkw > 0) {
			return summeAlter / anzahlPkw;
		} else {
			throw new IllegalArgumentException(
					"Not Available! ");
		}
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	  * Methode gibt den Durschnittsalter der Lkw zuruck
	 * @return int alterLkw
 	 */
	public int durchschnittsalterLkw() {
		int summeAlter = 0;
		int anzahlLkw = 0;

		if (dao.getFahrzeugList() != null) {
			for (Fahrzeug fahrzeug : dao.getFahrzeugList()) {
				if (fahrzeug instanceof Lkw) {
					summeAlter += fahrzeug.getAlter();
					anzahlLkw++;
				}
			}
		} else {
			throw new IllegalArgumentException(
					"Not Available! ");
		}
		if (anzahlLkw > 0) {
			return summeAlter / anzahlLkw;
		} else {
			throw new IllegalArgumentException(
					"Not Available! ");
		}
		
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 *  Methode gibt den Durschnittpreis der Fahrzeug zuruck
	 * @return int preis
	 */
	public int durchschnittspreisFahrzeuge() {
		int durchschnittsPreis = 0;

		if (dao.getFahrzeugList() != null) {
			for (Fahrzeug fahrzeug : dao.getFahrzeugList()) {
				durchschnittsPreis += fahrzeug.getPreis();
				
			}
		} else {
			throw new IllegalArgumentException(
					"Not Available! ");
		}
		return durchschnittsPreis / dao.getFahrzeugList().size();
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	  *  Methode gibt den Durschnittpreis der Lkw zuruck
	 * @return int preisLkw
	 */
	public int durchschnittspreisLkw() {
		int durchschnittsPreis = 0;
		int anzahlLkw = 0;

		if (dao.getFahrzeugList() != null) {
			for (Fahrzeug fahrzeug : dao.getFahrzeugList()) {
				if (fahrzeug instanceof Lkw) {
					durchschnittsPreis += fahrzeug.getPreis();
					anzahlLkw++;
				}
			}
		} else {
			throw new IllegalArgumentException(
					"Not Available! ");
		}

		if (anzahlLkw > 0) {
			return durchschnittsPreis / anzahlLkw;
		} else {
			throw new IllegalArgumentException(
					"Not Available! ");
		}

	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 *  Methode gibt den Durschnittpreis der Pkw zuruck
	 * @return int preisPkw
	 */
	public int durchschnittspreisPkw() {
		int durchschnittsPreis = 0;
		int anzahlPkw = 0;

		if (dao.getFahrzeugList() != null) {
			for (Fahrzeug fahrzeug : dao.getFahrzeugList()) {
				if (fahrzeug instanceof Pkw) {
					durchschnittsPreis += fahrzeug.getPreis();
					anzahlPkw++;
				}
			}
		} else {
			throw new IllegalArgumentException(
					"Not Available! ");
		}
		if (anzahlPkw > 0) {
			return durchschnittsPreis / anzahlPkw;
		} else {
			throw new IllegalArgumentException(
					"Not Available! ");
		}

	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 *  *  Methode gibt den Altesten Fahrzeug zuruck
	 * @return Fahrzeug fahrzeug
	 */
	public Fahrzeug altesteFahrzeug() {
		int altesteFahrzeug = 0;
		int idFahrzeug = 0;

		if (dao.getFahrzeugList() != null) {
			for (Fahrzeug fahrzeug : dao.getFahrzeugList()) {
				if (fahrzeug.getAlter() > altesteFahrzeug) {
					altesteFahrzeug = fahrzeug.getAlter();
					idFahrzeug = fahrzeug.getId();
				}
			}
		} else {
			throw new IllegalArgumentException(
					"Not Available! ");
		}

		return dao.getFahrzeugbyId(idFahrzeug);

	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * Methode gibt den Altesten Pkw zuruck
	 * @return Fahrzeug fahrzeug
	 */
	public Fahrzeug altestePkw() {
		int altesteFahrzeug = 0;
		int idFahrzeug = 0;

		if (dao.getFahrzeugList() != null) {
			for (Fahrzeug fahrzeug : dao.getFahrzeugList()) {
				if (fahrzeug instanceof Pkw) {
					if (fahrzeug.getAlter() > altesteFahrzeug) {
						altesteFahrzeug = fahrzeug.getAlter();
						idFahrzeug = fahrzeug.getId();
					}
				}
			}
		} else {
			throw new IllegalArgumentException(
					"Not Available! ");
		}

		if (altesteFahrzeug > 1) {
			return dao.getFahrzeugbyId(idFahrzeug);
		} else {
			throw new IllegalArgumentException(
					"Not Available! ");
		}

	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * Methode gibt den Altesten Lkw zuruck
	 * @return Fahrzeug fahrzeug
	 * @return
	 */
	public Fahrzeug altesteLkw() {
		int altesteFahrzeug = 0;
		int idFahrzeug = 0;

		if (dao.getFahrzeugList() != null) {
			for (Fahrzeug fahrzeug : dao.getFahrzeugList()) {
				if (fahrzeug instanceof Lkw) {
					if (fahrzeug.getAlter() > altesteFahrzeug) {
						altesteFahrzeug = fahrzeug.getAlter();
						idFahrzeug = fahrzeug.getId();
					}
				}
			}
		} else {
			throw new IllegalArgumentException(
					"Not Available! ");
		}

		if (altesteFahrzeug > 1) {
			return dao.getFahrzeugbyId(idFahrzeug);
		} else {
			throw new IllegalArgumentException(
					"Es gibts noch keine Lkw gespeichert! ");
		}
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * Gibt fahrzeug zuruck anhand id,
	 * habe das fur Servlets benutzt
	 * @param id
	 * @return Fahrzeug fahrzeug
	 */
	public Fahrzeug getFahrzeugById(int id) {
		return dao.getFahrzeugbyId(id);
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * Gibt liste zuruck,
	 *habe das fur Servlets benutzt
	 * @return ArrayList<Fahrzeug> 
	 */
	public ArrayList<Fahrzeug> getFahrzeugListe() {
		return dao.getFahrzeugList();
	}
}
