package fahrzeuge;

import java.util.ArrayList;
import java.util.Date;

import utils.Umwandeln;

public class Pkw extends Fahrzeug {

	private ArrayList<String> ausstattungspakete;

	/**
	 * parameter constructor
	 * 
	 * @param marke
	 * @param modell
	 * @param baujahr
	 * @param leistung
	 * @param kilometerstand
	 * @param grundpreis
	 * @param id
	 * @param uberprufungsdatum
	 * @param uberprufungsinterval
	 * @param ausstattungspakete
	 */
	public Pkw(String marke, String modell, Date baujahr, int leistung,
			int kilometerstand, int grundpreis, int id, Date uberprufungsdatum,
			int uberprufungsinterval, ArrayList<String> ausstattungspakete) {

		super(marke, modell, baujahr, leistung, kilometerstand, grundpreis, id,
				uberprufungsdatum, uberprufungsinterval);
		this.ausstattungspakete = ausstattungspakete;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * gibt die ausstattungspakete des Fahrzeuges-Pkw zuruck
	 * 
	 * @return ArrayList<String> ausstattungspakete
	 */
	public ArrayList<String> getAusstattungspakete() {
		return ausstattungspakete;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * setzt die ausstattungspakete des Fahrzeuges-Pkw ein
	 * 
	 * @param ArrayList
	 *            <String> ausstattungspakete
	 */
	public void setAusstattungspakete(ArrayList<String> ausstattungspakete) {
		if (ausstattungspakete == null || ausstattungspakete.isEmpty()) {
			throw new IllegalArgumentException(
					"Ausstatung darf nicht null, Empty or kleiner als 2 Zeichnen sein! ");
		}
		String newMarke = "";

		for (int j = 0; j < ausstattungspakete.size(); j++) {
			String aktuelleAusstatung = ausstattungspakete.get(j);
			for (int i = 0; i < aktuelleAusstatung.length(); i++) {
				char original = aktuelleAusstatung.charAt(i);
				char copy = Character.toLowerCase(original);

				if (copy < 'a' || copy > 'z')
					throw new IllegalArgumentException(
							"Ausstatung darf nur zeichnen A-Z enthalten!");
				newMarke += original;
			}
			ausstattungspakete.add(newMarke);
		}
		this.ausstattungspakete = ausstattungspakete;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@Override
	public int getRabatt() {
		int paketGrosse = getAusstattungspakete().size();
		if (paketGrosse < 5) {
			return (getGrundpreis() / 100) * (10 - paketGrosse);
		}

		return (getGrundpreis() / 100) * 10;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@Override
	public String toString() {
		return "Marke : " + getMarke() + "\nModell : " + getModell()
				+ "\nBaujahr : " + Umwandeln.DateToString(getBaujahr())
				+ "\nLeistung : " + getLeistung() + "\nKilometerstand : "
				+ getKilometerstand() + "\nGrundpreis : " + getGrundpreis()
				+ "\nId : " + getId() + "\nUberprufungsdatum : "
				+ Umwandeln.DateToString(getUberprufungsdatum())
				+ "\nUberprufungsinterval : " + getUberprufungsinterval();
	}

}
