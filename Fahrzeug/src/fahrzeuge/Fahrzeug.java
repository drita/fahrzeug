package fahrzeuge;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import utils.Umwandeln;

public abstract class Fahrzeug implements Serializable {

	private String marke;
	private String modell;
	private Date baujahr;
	private int leistung;
	private int kilometerstand;
	private int grundpreis;
	private int id;
	private Date uberprufungsdatum;
	private int uberprufungsinterval;
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * parameter constructor
	 * 
	 * @param String
	 *            marke
	 * @param String
	 *            modell
	 * @param Date
	 *            baujahr
	 * @param int leistung
	 * @param int kilometerstand
	 * @param int grundpreis
	 * @param int id
	 * @param Date
	 *            uberprufungsdatum
	 * @param int uberprufungsinterval
	 */
	public Fahrzeug(String marke, String modell, Date baujahr, int leistung,
			int kilometerstand, int grundpreis, int id, Date uberprufungsdatum,
			int uberprufungsinterval) {
		setMarke(marke);
		setModell(modell);
		setBaujahr(baujahr);
		setLeistung(leistung);
		setKilometerstand(kilometerstand);
		setGrundpreis(grundpreis);
		setId(id);
		setUberprufungsdatum(uberprufungsdatum);
		setUberprufungsinterval(uberprufungsinterval);
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * gibt den Leistung des Fahrzeugs zuruck
	 * 
	 * @return int leistung
	 */
	public int getLeistung() {
		return leistung;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * gibt den Kilometerstand des Fahrzeugs zuruck
	 * 
	 * @return int kilometerstand
	 */
	public int getKilometerstand() {
		return kilometerstand;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * gibt den baujahr des Fahrzeugs zuruck
	 * 
	 * @return Date baujahr
	 */
	public Date getBaujahr() {

		return baujahr;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * gibt den modell des Fahrzeugs zuruck
	 * 
	 * @return String modell
	 */
	public String getModell() {
		return modell;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * gibt den modell des Fahrzeugs zuruck
	 * 
	 * @return int uberprufungsinterval
	 */
	public int getUberprufungsinterval() {
		return uberprufungsinterval;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * gibt den modell des Fahrzeugs zuruck
	 * 
	 * @return int id
	 */
	public int getId() {
		return id;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * gibt den modell des Fahrzeugs zuruck
	 * 
	 * @return Date uberprufungsdatum
	 */
	public Date getUberprufungsdatum() {

		return uberprufungsdatum;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * gibt den modell des Fahrzeugs zuruck
	 * 
	 * @return int grundpreis
	 */
	public int getGrundpreis() {
		return grundpreis;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * gibt den modell des Fahrzeugs zuruck
	 * 
	 * @return String marke
	 */
	public String getMarke() {
		return marke;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * gibt den naechstePrufung des Fahrzeugs zuruck anhand
	 * uberprufungsintervall
	 * 
	 * @return Date naechstePrufung
	 */
	public Date getNaechstePrufung() {
		int aktuelleJahr = Calendar.getInstance().get(Calendar.YEAR);

		Calendar nachsteUberprufungs = Umwandeln
				.DateToCalendar(getUberprufungsdatum());

		while (nachsteUberprufungs.get(Calendar.YEAR) < (aktuelleJahr + getUberprufungsinterval())) {
			nachsteUberprufungs.add(Calendar.YEAR, getUberprufungsinterval());
		}
		return Umwandeln.CalendarToDate(nachsteUberprufungs);
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * gibt den alter des Fahrzeugs zuruck
	 * 
	 * @return int alter
	 */
	public int getAlter() {
		Date date = new Date();
		int alter = Umwandeln.getYearVonDate(date)
				- Umwandeln.getYearVonDate(baujahr);
		return alter;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * gibt den preis des Fahrzeugs zuruck
	 * 
	 * @return int preis
	 */
	public int getPreis() {
		int rabatProzent = 0;
		if (this instanceof Lkw) {
			rabatProzent = 90;
		} else {
			rabatProzent = 95;
		}
		if ((getRabatt() * getAlter()) < (getGrundpreis() / 100) * rabatProzent)
			return grundpreis - (getRabatt() * getAlter());

		return grundpreis - ((getGrundpreis() / 100) * rabatProzent);
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * gibt den rabatt zuruck abhangig von Fahrzeug typ
	 * 
	 * @return
	 */
	public abstract int getRabatt();

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * setzt den marke des Fahrzeugs ein
	 * 
	 * @param String
	 *            marke, darf nicht null, empty or <2 sein, nur zeichnen A - Z
	 */
	public void setMarke(String marke) {
		if (marke == null || marke.isEmpty() || marke.length() < 2) {
			throw new IllegalArgumentException(
					"Marke darf nicht null, Empty or kleiner als 2 Zeichnen sein! ");
		}
		String newMarke = "";
		for (int i = 0; i < marke.length(); i++) {
			char original = marke.charAt(i);
			char copy = Character.toLowerCase(original);

			if (copy < 'a' || copy > 'z')
				throw new IllegalArgumentException(
						"Marke darf nur zeichnen A-Z enthalten!");
			newMarke += original;
		}
		this.marke = newMarke;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * setzt den modell des Fahrzeugs ein
	 * 
	 * @param String
	 *            modell, darf nicht null, Empty or < 2 sein
	 */
	public void setModell(String modell) {

		if (modell == null || modell.isEmpty() || modell.length() < 2) {
			throw new IllegalArgumentException(
					"Modell darf nicht null, Empty or kleiner als 2 Zeichnen sein! ");
		}

		this.modell = modell;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * setzt den baujahr des Fahrzeugs ein
	 * 
	 * @param Date
	 *            baujahr, darf nicht in zukunft liegen
	 */
	public void setBaujahr(Date baujahr) {
		Date aktuelleDatum = new Date();
		if (baujahr == null || baujahr.after(aktuelleDatum)) {
			throw new IllegalArgumentException(
					"Bitte geben sie richtige baujahr an");
		}

		this.baujahr = baujahr;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * setzt den leistung des Fahrzeugs ein
	 * 
	 * @param int leistung
	 */
	public void setLeistung(int leistung) {
		if (leistung < 30) {
			throw new IllegalArgumentException(
					"Leistung kann nicht kleiner als 30 sein");
		}
		this.leistung = leistung;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * setzt den Kilometerstand des Fahrzeugs ein
	 * 
	 * @param int kilometerstand
	 */
	public void setKilometerstand(int kilometerstand) {
		if (kilometerstand < 1) {
			throw new IllegalArgumentException(
					"Bitte geben sie Kilometerstand ein");
		}
		this.kilometerstand = kilometerstand;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * setzt den grundpreis des Fahrzeugs ein
	 * 
	 * @param int grundpreis
	 */
	public void setGrundpreis(int grundpreis) {
		if (grundpreis < 100) {
			throw new IllegalArgumentException(
					"Preis kann nicht kleiner als 100 Euro Sein");
		}
		this.grundpreis = grundpreis;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * setzt den id des Fahrzeugs ein
	 * 
	 * @param int id
	 */
	public void setId(int id) {

		if (id != (int) id) {
			throw new IllegalArgumentException("Id muss von Typ integer sein");
		}
		this.id = id;

	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * setzt den uberprufungsdatum des Fahrzeugs ein
	 * 
	 * @param Date
	 *            uberprufungsdatum
	 */
	public void setUberprufungsdatum(Date uberprufungsdatum) {
		Date aktuelleDatum = new Date();

		if (uberprufungsdatum == null){
			throw new IllegalArgumentException(
					"Uberprufungsdatum ist leer");
		}
		if( uberprufungsdatum.after(aktuelleDatum)){
			throw new IllegalArgumentException(
					"Uberprufungsdatum kann nicht in zukunft sein");
		}
		if(uberprufungsdatum.before(getBaujahr())) {
			throw new IllegalArgumentException(
					"Uberprufungsdatum kann nicht vor Baujahr sein");
		}
	

		this.uberprufungsdatum = uberprufungsdatum;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * setzt den uberprufungsinterval des Fahrzeugs ein
	 * 
	 * @param int uberprufungsinterval
	 */
	public void setUberprufungsinterval(int uberprufungsinterval) {
		if (grundpreis < 1) {
			throw new IllegalArgumentException(
					"Uberprufungs interval soll mindestenst 1 jahr sein");
		}

		this.uberprufungsinterval = uberprufungsinterval;
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	public abstract String toString();
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
}
