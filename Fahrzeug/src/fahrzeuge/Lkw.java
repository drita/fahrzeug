package fahrzeuge;

import java.util.ArrayList;
import java.util.Date;

import utils.Umwandeln;

public class Lkw extends Fahrzeug{
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++		
	/**
	 * default constructor
	 */
	public Lkw(String marke, String modell, Date baujahr, int leistung,
			int kilometerstand, int grundpreis, int id, Date uberprufungsdatum,
			int uberprufungsinterval) {
		
		super(marke,  modell,  baujahr,  leistung, kilometerstand,  grundpreis,  id,  uberprufungsdatum,
				 uberprufungsinterval);
		
	}

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++		
	@Override
	public int getRabatt() {
		
		return (getGrundpreis() / 100) *5;
	}
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++		
	@Override
	public String toString() {
		return  "Marke : " + getMarke()
				+ "\nModell : " + getModell()
				+ "\nBaujahr : " + Umwandeln.DateToString(getBaujahr())
				+ "\nLeistung : " + getLeistung()
				+ "\nKilometerstand : " + getKilometerstand()
				+ "\nGrundpreis : " + getGrundpreis()
				+ "\nId : " + getId()
				+ "\nUberprufungsdatum : " + Umwandeln.DateToString(getUberprufungsdatum())
				+ "\nUberprufungsinterval : " + getUberprufungsinterval();
	}
}
