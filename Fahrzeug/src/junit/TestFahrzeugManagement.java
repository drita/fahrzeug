package junit;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import management.FahrzeugManagement;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import dao.SerializedFahrzeugDAO;
import fahrzeuge.*;
import utils.ConfigurationDb;
import utils.Umwandeln;

public class TestFahrzeugManagement {
	FahrzeugManagement fahrzeugManagement;

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * Daten hinzufugen zum prufen vor jedem Test
	 */
	@Before
	public void FahrzeugeHinzufgenTest() {
		fahrzeugManagement = new FahrzeugManagement(new SerializedFahrzeugDAO(
				ConfigurationDb.fahrzeugDBPath));
		einfuge5Pkw();
		einfuge5Lkw();
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * daten loschen nach jedem Test
	 * 
	 * @throws IOException
	 */
	@After
	public void LoscheTestDatenFahrzeuge() throws IOException {
		new FileOutputStream(ConfigurationDb.fahrzeugDBPath).close();
		File file = new File(ConfigurationDb.fahrzeugDBPath);
		file.delete();
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@Test
	public void alleDatenAusgebenTest() {

		ArrayList<String> austattungspakete = new ArrayList<String>();
		austattungspakete.add("Klima Anlage");
		austattungspakete.add("GPS inkl Navigation");
		austattungspakete.add("Digitales Radio");

		Fahrzeug gesuchtPkw = new Pkw("Bmw", "A8",
				Umwandeln.StringToDate("23.06.2008"), 65, 12000, 1000, 2,
				Umwandeln.StringToDate("23.06.2010"), 2, austattungspakete);

		assertEquals(gesuchtPkw.toString(),
				fahrzeugManagement.alleDatenAusgeben(fahrzeugManagement
						.getFahrzeugListe().get(1)));
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@Test
	public void fahrzeugDatenAndernTest() {
		Fahrzeug fahrzeug = fahrzeugManagement.getFahrzeugListe().get(2);
		fahrzeug.setMarke("NeueMerzeced");
		fahrzeug.setModell("neueA8");
		fahrzeugManagement.fahrzeugDatenAndern(fahrzeug);

		assertEquals(fahrzeug.toString(),
				fahrzeugManagement.alleDatenAusgeben(fahrzeugManagement
						.getFahrzeugListe().get(9)));
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@Test(expected = IllegalArgumentException.class)
	public void fahrzeugDatenAndernExceptionTest() {

		ArrayList<String> austattungspakete = new ArrayList<String>();
		austattungspakete.add("Klima Anlage");
		austattungspakete.add("GPS inkl Navigation");
		austattungspakete.add("Digitales Radio");

		Fahrzeug fahrzeug = new Pkw("Audi", "A8 D4/4H",
				Umwandeln.StringToDate("23.06.2012"), 65, 12000, 1000, 13,
				Umwandeln.StringToDate("23.06.2006"), 3, austattungspakete);
		
		
		fahrzeugManagement.fahrzeugDatenAndern(fahrzeug);

		assertEquals(fahrzeug.toString(),
				fahrzeugManagement.alleDatenAusgeben(fahrzeugManagement
						.getFahrzeugListe().get(4)));
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@Test
	public void gesamtzahlErfasstenFahrzeugeTest() {

		assertEquals(10, fahrzeugManagement.gesamtzahlErfasstenFahrzeuge());
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@Test
	public void gesamtzahlErfasstenPkwTest() {

		assertEquals(5, fahrzeugManagement.gesamtzahlErfasstenPkw());
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@Test
	public void gesamtzahlErfasstenLkwTest() {
	assertEquals(5, fahrzeugManagement.gesamtzahlErfasstenLkw());
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@Test
	public void durchschnittsalterFahrzeugeTest() {

		assertEquals(9, fahrzeugManagement.durchschnittsalterFahrzeuge());
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@Test
	public void durchschnittsalterPkwTest() {

		assertEquals(9, fahrzeugManagement.durchschnittsalterPkw());
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@Test
	public void durchschnittsalterLkwTest() {
		assertEquals(9, fahrzeugManagement.durchschnittsalterLkw());
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@Test
	public void durchschnittspreisFahrzeugeTest() {

		 assertEquals(450,fahrzeugManagement.durchschnittspreisFahrzeuge());
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@Test
	public void durchschnittspreisPkwTest() {

		assertEquals(370, fahrzeugManagement.durchschnittspreisPkw());
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@Test
	public void durchschnittspreisLkwTest() {

		assertEquals(530, fahrzeugManagement.durchschnittspreisLkw());
	}
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@Test
	public void altesteFahrzeug() {

		assertEquals(fahrzeugManagement.getFahrzeugListe().get(9), fahrzeugManagement.altesteFahrzeug());
	}
	
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@Test
	public void altestePkw() {

		assertEquals(fahrzeugManagement.getFahrzeugListe().get(4), fahrzeugManagement.altestePkw());
	}
	
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	@Test
	public void altesteLkw() {

		assertEquals(fahrzeugManagement.getFahrzeugListe().get(9), fahrzeugManagement.altesteLkw());
	}
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/**
	 * Das falsche datum einfugen, ParseException erwartet
	 */
	@Test(expected=IllegalArgumentException.class)
	public void datumEingabePrufen(){
		ArrayList<String> austattungspakete = new ArrayList<String>();
		austattungspakete.add("Klima Anlage");
		austattungspakete.add("GPS inkl Navigation");
		austattungspakete.add("Digitales Radio");

		Fahrzeug pkw1 = new Pkw("Audi", "A8 D4/4H",
				Umwandeln.StringToDate("23.06.2012"), 65, 12000, 1000, 1,
				Umwandeln.StringToDate("23.06.2006"), 3, austattungspakete);
		fahrzeugManagement.fahrzeugeHinzufugen(pkw1);
	}
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	private void einfuge5Pkw() {

		ArrayList<String> austattungspakete = new ArrayList<String>();
		austattungspakete.add("Klima Anlage");
		austattungspakete.add("GPS inkl Navigation");
		austattungspakete.add("Digitales Radio");

		Fahrzeug pkw1 = new Pkw("Audi", "A8 D4/4H",
				Umwandeln.StringToDate("23.06.2010"), 65, 12000, 1000, 1,
				Umwandeln.StringToDate("23.06.2012"), 2, austattungspakete);
		fahrzeugManagement.fahrzeugeHinzufugen(pkw1);

		Fahrzeug pkw2 = new Pkw("Bmw", "A8",
				Umwandeln.StringToDate("23.06.2008"), 65, 12000, 1000, 2,
				Umwandeln.StringToDate("23.06.2010"), 2, austattungspakete);
		fahrzeugManagement.fahrzeugeHinzufugen(pkw2);

		Fahrzeug pkw3 = new Pkw("Mercedes", "A8",
				Umwandeln.StringToDate("23.06.2006"), 65, 1000, 1000, 3,
				Umwandeln.StringToDate("23.06.2008"), 2, austattungspakete);
		fahrzeugManagement.fahrzeugeHinzufugen(pkw3);

		Fahrzeug pkw4 = new Pkw("Citroen", "A8",
				Umwandeln.StringToDate("23.06.2004"), 65, 1000, 1000, 4,
				Umwandeln.StringToDate("23.06.2006"), 2, austattungspakete);
		fahrzeugManagement.fahrzeugeHinzufugen(pkw4);

		Fahrzeug pkw5 = new Pkw("Golf", "A8",
				Umwandeln.StringToDate("23.06.2002"), 65, 1000, 1000, 5,
				Umwandeln.StringToDate("23.06.2004"), 2, austattungspakete);
		fahrzeugManagement.fahrzeugeHinzufugen(pkw5);
	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	private void einfuge5Lkw() {

		Fahrzeug lkw1 = new Lkw("Audi", "A8 D4/4H",
				Umwandeln.StringToDate("23.06.2010"), 65, 12000, 1000, 8,
				Umwandeln.StringToDate("23.06.2012"), 2);
		fahrzeugManagement.fahrzeugeHinzufugen(lkw1);

		Fahrzeug lkw2 = new Lkw("Bmw", "A8",
				Umwandeln.StringToDate("23.06.2008"), 65, 12000, 1000, 9,
				Umwandeln.StringToDate("23.06.2010"), 2);
		fahrzeugManagement.fahrzeugeHinzufugen(lkw2);

		Fahrzeug lkw3 = new Lkw("Mercedes", "A8",
				Umwandeln.StringToDate("23.06.2006"), 65, 12000, 1000, 10,
				Umwandeln.StringToDate("23.06.2008"), 2);
		fahrzeugManagement.fahrzeugeHinzufugen(lkw3);

		Fahrzeug lkw4 = new Lkw("Citroen", "A8",
				Umwandeln.StringToDate("23.06.2004"), 65, 12000, 1000, 11,
				Umwandeln.StringToDate("23.06.2006"), 2);
		fahrzeugManagement.fahrzeugeHinzufugen(lkw4);

		Fahrzeug lkw5 = new Lkw("Golf", "A8",
				Umwandeln.StringToDate("23.06.2000"), 65, 12000, 1000, 12,
				Umwandeln.StringToDate("23.06.2004"), 2);
		fahrzeugManagement.fahrzeugeHinzufugen(lkw5);
	}
}
