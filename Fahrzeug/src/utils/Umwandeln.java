package utils;

import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Umwandeln {

	public static Calendar DateToCalendar(Date date) {

		Calendar calendar = Calendar.getInstance();

		calendar.setTime(date);
		return calendar;
	}

	public static Date CalendarToDate(Calendar calendar) {
		
	    Date utilDate = calendar.getTime();
	    
		return utilDate;
	}

	public static int getYearVonDate(Date date) {
		
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(date);
	    
		return cal.get(Calendar.YEAR);
	}

	public static String DateToString(Date date) {
		Format formatter = new SimpleDateFormat("dd.MM.yyyy");
		return formatter.format(date);
	}

	public static Date StringToDate(String string){
		DateFormat format = new SimpleDateFormat("dd.MM.yyyy");
		
		try {
			return format.parse(string);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
